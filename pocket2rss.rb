#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pathname'
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../gems.rb', Pathname.new(__FILE__).realpath)
require 'rubygems'
require 'bundler/setup'

require 'open-uri'
require 'nokogiri'
require 'uri'
require 'pry'
require 'cgi'
require 'chronic'
require 'time'
require 'rss'
require 'aws-sdk-s3'
require 'logger'
require 'zlib'
require 'bugsnag'

# Settings

USER = ENV.fetch('USER')
S3_BUCKET = ENV.fetch('S3_BUCKET')
BUGSNAG_API_KEY = ENV['BUGSNAG_API_KEY']

LOGGER = Logger.new(STDOUT)

# Set time zone for app to UTC, so chronic uses it
# Kinda meh...
ENV['TZ'] = 'UTC'

# Setup bugsnag, if available
if BUGSNAG_API_KEY
  at_exit do
    if $!
      Bugsnag.notify($!)
    end
  end
  LOGGER.info 'Bugsnag enabled'
else
  LOGGER.warn 'Bugsnag disabled, BUGSNAG_API_KEY not defined'
end

class PocketScraper
  def entries
    Nokogiri::HTML(open("https://getpocket.com/@#{USER}"))
      .xpath("//div[contains(@class, 'sprofile-post ')]")
      .map do |entry|
        raw_time = entry.xpath("div/a[@class='sprofile-post-time']").inner_text
        time = parse_time(raw_time)

        entry_content = entry.xpath("div[@class='sprofile-post-article']")

        raw_url = entry_content.xpath("a[@class='sprofile-article-link']/@href").first&.value

        url = clean_url(extract_url(raw_url))

        raw_title = entry_content.xpath("div/h3").inner_text
        title = prettify_title(raw_title, url)

        {
          title: title,
          url: url,
          time: time,
          raw: {
            title: raw_title,
            url: raw_url,
            time: raw_time,
          },
        }
    end
  end

  private

  # Pocket keeps urls in format
  # https://getpocket.com/redirect?&url=encoded_url&h=some_hash_value
  # This method extracts the encoded_url and decodes it
  def extract_url(pocket_url)
    URI.unescape(pocket_url.match(/https:\/\/getpocket.com\/redirect\?&url=(.*)&h=/)[1])
  end

  # Quick hack to remove tracking stuff from url
  # see http://navitronic.co.uk/2010/12/removing-google-analytics-cruft-from-urls/
  def clean_url(url)
    url
      .gsub(/(\?|\&)?utm_[a-z]+=[^\&]+/, '')
  end

  def prettify_title(raw_title, url)
    if raw_title.empty? || (raw_title.start_with?('{{') && raw_title.end_with?('}}'))
      # title seems broken, use simplified url instead
      url
        .split('?').first
        .sub('https://', '')
        .sub('http://', '')
    else
      # title can have html entities, let's make sure we decode them
      CGI.unescapeHTML(raw_title)
    end
  end

  def parse_time(raw_time)
    Chronic.parse(raw_time)
  end
end

class EntriesToRss
  def convert(entries)
    RSS::Maker.make('2.0') do |maker|
      maker.channel.author = USER
      maker.channel.updated = Time.now.to_s
      maker.channel.link = maker.channel.about = "https://getpocket.com/@#{USER}"
      maker.channel.description = maker.channel.title = "Pocket feed for #{USER}"

      entries.each do |title:, url:, time:, **_|
        maker.items.new_item do |item|
          item.link = url
          item.title = title
          item.updated = time.strftime('%Y/%m/%d').to_s
        end
      end
    end
  end
end

class GZipper
  def gzip(data)
    sio = StringIO.new
    gz = Zlib::GzipWriter.new(sio, Zlib::BEST_COMPRESSION)
    gz.write(data)
    gz.close
    sio.string
  end
end

LOGGER.info "Started execution, going to fetch feeds"

entries = PocketScraper.new.entries

LOGGER.info "Fetched #{entries.count} entries"

LOGGER.info "Generating RSS..."

rss_feed = EntriesToRss.new.convert(entries).to_s

LOGGER.info "Generated RSS successfully (#{rss_feed.size}) characters"

LOGGER.info "Gzipping feed..."

compressed_rss_feed = GZipper.new.gzip(rss_feed)
raise 'Error compressing feed' unless compressed_rss_feed.size > 0

LOGGER.info "Generated gzipped RSS successfully (#{compressed_rss_feed.size}) bytes"

LOGGER.info "Uploading to S3..."

Aws::S3::Resource.new.bucket(S3_BUCKET).put_object(
  key: 'pocketfeed/pocket.xml',
  body: compressed_rss_feed,
  content_type: 'application/rss+xml',
  content_encoding: 'gzip',
)

LOGGER.info "Uploaded to S3 successfully. All finished!"
