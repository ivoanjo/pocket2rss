source 'https://rubygems.org'

gem 'nokogiri'
gem 'pry'
gem 'chronic'
gem 'aws-sdk-s3'
gem 'bugsnag'
